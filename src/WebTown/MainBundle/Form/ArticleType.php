<?php

namespace WebTown\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title','text',array('label' => 'Cím','attr'=>array('class'=>'inputClass')))
            ->add('slug','text',array('label'=>'URL','attr'=>array('class'=>'inputClass')))
            ->add('description','textarea',array('label'=>'Bevezető','attr'=>array('class'=>'inputClass')))
            ->add('content','textarea',array('label'=>'Tartalom','required' => false,'attr'=>array('class'=>'tynmc')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebTown\MainBundle\Entity\Article'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'webtown_mainbundle_article';
    }
}
